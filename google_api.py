from __future__ import print_function
from prettytable import PrettyTable
import httplib2
import os
import sys
import config
import time

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient import errors
from file import File

class GoogleApi():

    user_1_email = ''
    user_2_email = ''

    def get_credentials(self, cred_file):
        home_dir = os.path.dirname(os.path.abspath(__file__))
        credential_dir = os.path.join(home_dir, config.CREDENTIALS_DIR)
        print ('Credentials dir: {0}'.format(credential_dir))
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir, cred_file)

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            print ('Credentials not found or invalid')
            flow = client.flow_from_clientsecrets(config.CLIENT_SECRET_FILE, config.SCOPES)
            flow.user_agent = config.APPLICATION_NAME
            credentials = tools.run_flow(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def __init__(self, cred_path, user_1, user_2 = ''):

        self.user_1_email = user_1
        self.user_2_email = user_2

        self.File = File(self.user_1_email)

        credentials = self.get_credentials(cred_path)
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('drive', 'v2', http=http)

        results = self.retrieve_all_files(service)
        parsedListPermissions = self.parseResult(service, results)

        self.processingDomains(parsedListPermissions)

    def retrieve_all_files(self, service):
      result = []
      page_token = None
      number = 0

      print ('Files found:')
      while True:
        try:
          param = {}
          param['q'] = "'" + self.user_1_email + "' in owners"
          param['orderBy'] = "title"
          if page_token:
            param['pageToken'] = page_token
          files = service.files().list(**param).execute()

          result.extend(files['items'])
          page_token = files.get('nextPageToken')

          number += len(files['items'])
          print (number)

          if not page_token:
            break
        except errors.HttpError as error:
          print ('An error occurred: %s', error)
          break
      return result

    def parseResult(self, service, listFiles):
        number = 1
        dictFilesPermissions = {}
        ownerName = ''

        if len(listFiles) > 0:
            ownerName = listFiles[0]['ownerNames'][0].encode('utf-8')

        self.File.write('Name: {0}\tEmail:{1}\n'.format(ownerName, self.user_1_email))
        self.File.write('\nALL FILES\n')
        tableAllFiles = PrettyTable()

        tableAllFiles.field_names = ["Number", "FileId", "FileName", "Created Date", "Size", "Trashed"]

        print ('Files processed:')
        for file in listFiles:
            sys.stdout.write("\r" + str(number))
            sys.stdout.flush()
            time.sleep(0.1)

            id = 'Unknown'
            if 'id' in file:
                id = file['id']

            title = 'Unknown'
            if 'title' in file:
                title = file['title'].encode('utf-8')

            date = 'Unknown'
            if 'createdDate' in file:
                date = file['createdDate']

            fileSize = 'Unknown'
            if 'fileSize' in file:
                fileSize = file['fileSize']

            trashed = ''
            if 'trashed' in file['labels']:
                if file['labels']['trashed'] == True:
                    trashed = '+'

            tableAllFiles.add_row([str(number) + '.', id, title, date, fileSize, trashed])

            listFilePermissions = []
            listPermissions = self.retrieve_permissions(service, file['id'])

            for permission in listPermissions:

                role = 'Unknown'
                if 'role' in permission:
                    role = permission['role']

                additionalRoles = ''
                if 'additionalRoles' in permission:
                    for additionalRole in permission['additionalRoles']:
                        additionalRoles += additionalRole + ' '

                userName = 'Unknown'
                if 'name' in permission:
                    userName = permission['name'].encode('utf-8')

                userEmail = 'Unknown'
                if 'emailAddress' in permission:
                    userEmail = permission['emailAddress']

                domain = 'Unknown'
                if 'domain' in permission:
                    domain = permission['domain']

                fileName = 'Unknown'
                if 'title' in file:
                    fileName = file['title'].encode('utf-8')

                listPermission = {}
                listPermission['id'] = id
                listPermission['fileName'] = fileName
                listPermission['userEmail'] = userEmail
                listPermission['userName'] = userName
                listPermission['domain'] = domain
                listPermission['role'] = role
                listPermission['additionalRoles'] = additionalRoles

                listFilePermissions.append(listPermission)

            dictFilesPermissions[file['id']] = listFilePermissions
            number += 1

        self.File.write(str(tableAllFiles))
        print ()
        return dictFilesPermissions

    def retrieve_permissions(self, service, file_id):
      try:
        permissions = service.permissions().list(fileId=file_id).execute()
        return permissions.get('items', [])
      except errors.HttpError as error:
          self.File.write ('An error occurred: %s.\n', error)
      return None

    def processingDomains(self, listFilesPermissions):
        tableOtherDomains = PrettyTable()
        tableToPerson = PrettyTable()
        tableOtherDomains.field_names  = ["Number", "FileId", "FileName", "Email", "UserName", "Domain", "Role", "AdditionalRole"]
        tableToPerson.field_names  = ["Number", "FileId", "FileName", "Email", "UserName", "Domain", "Role", "AdditionalRole"]

        numberOtherDomain = 1
        numberOtherPerson = 1
        for file in listFilesPermissions:
            for permission in listFilesPermissions[file]:
                if permission['domain']!= 'awem.by':
                    tableOtherDomains.add_row([str(numberOtherDomain) + '.', permission['id'], permission['fileName'], permission['userEmail'], permission['userName'], permission['domain'], permission['role'], permission['additionalRoles']])
                    numberOtherDomain += 1

                if permission['userEmail'] == self.user_2_email or (self.user_2_email == '' and permission['role'] != 'owner'):
                    tableToPerson.add_row([str(numberOtherPerson) + '.', permission['id'], permission['fileName'], permission['userEmail'], permission['userName'], permission['domain'], permission['role'], permission['additionalRoles']])
                    numberOtherPerson += 1

        self.File.write('\n\nOTHER DOMAINS\n')
        self.File.write(str(tableOtherDomains))

        self.File.write('\n\nTO PERSON: {0}\n'.format(self.user_2_email))
        self.File.write(str(tableToPerson))

        self.File.printPath()