import config
import datetime
import os
import platform

class File():
    fullFilePath = ''
    def __init__(self, user_email):
        now = datetime.datetime.now()
        fileName = str(now.strftime("%Y-%m-%d(%H-%M-%S)-") + user_email + '.txt')
        self.createFolderIfNotExist()

        rootFolder = self.getRootFolder()
        self.fullFilePath = rootFolder + "/Results/" + fileName
        open(self.fullFilePath, 'w')

    def write(self, str):
        file = open(self.fullFilePath, 'a')
        file.write(str)


    def createFolderIfNotExist(self):
        folderPath = self.getRootFolder() + "/Results"
        if not os.path.exists(folderPath):
            os.makedirs(folderPath)

    def getRootFolder(self):
        return os.path.dirname(os.path.abspath(__file__))

    def printPath(self):
        print ('Saved to file: {0}'.format(self.fullFilePath))
